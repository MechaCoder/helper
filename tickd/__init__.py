import tickd.tickdAPI
import tickd.tickdUtils
#

class TickdCore():
    """ tickd core is a stand alone class for dealing with the tickd API """

    def __init__(self):
        self.TicketArray = []
        self.TickdArray = []
    # meathos for tickd
    def __str__(self):
        return "you are running {} instances, and have {} tickets saved".format(
            self.TickdArray,
            self.TicketArray
        )

    def loadTickdInstance(self, _id="", _url="", _key=""):
        """ loads a tickd instance into useable instances """
        if len(_id) != 0 and len(_url) != 0 and len(_key) != 0:
            self.TickdArray.append( {"id": _id, "url": _url, "key": _key})
            return True
        else:
            if len(_id) == 0:
                _id = str(input("pls enter the local id of the ticked > "))
            if len(_url) == 0:
                _url = str(input("pls enter the base URL > "))
            if len(_key) == 0:
                url = str(input("pls enter the api Acesses key > "))
            self.loadTickdInstance(_id, _url, _key)

    def readTickdInstance(self, _print=False):
        """ reads out the tickd objects if _print is true then tickd objects inline if False [default] returns the tickd array """
        if str(_print) == str(True):
            for t in self.TickdArray:
                print("{} - {}".format(t["id"], t["url"]) )
            return True
        else:
            return self.TickdArray

    def removeTickdInstance(self, _id=""):
        """ removes a tickd instance by it's id """
        if len(_id) != 0:
            self.readTickdInstance(True)
            res = tickdUtils.findArrayLocation(self.TickdArray, "id", _id)
            if str(res) != str(False):
                self.TickdArray.pop(int(res))
                return True
            else:
                self.removeTickdInstance()
        else:
            print("enter the id of the tickd instance you want remove")
            tId = str(input("tickd id > "))
            self.removeTickdInstance(tId)

    # tickets
    def getTicketfromAPI(self, _tickdid="", _ticketId=""):
        """ gets the ticket form the api and filters the object """
        if len(_tickdid) != 0 and len(_ticketId) != 0:
            tickdData = {}
            for e in self.TickdArray:
                if e["id"] == _tickdid:
                    tickdData = e
            if tickd.tickdUtils.valdateTickdInstance(tickdData):
                data = tickd.tickdAPI.getTicket( tickdData["url"], tickdData["key"], _ticketId )
                data = tickd.tickdUtils.filterTicketData(data, "{} - {}".format(_tickdid,_ticketId))
                return data
            else:
                self.getTicketfromAPI()
        else:
            if len(_tickdid) == 0:
                self.readTickdInstance()
                print("enter the id of the tickd you want to use")
                _tickdid = str(input(" > "))

            if len(_ticketId) == 0:
                print("enter ticket number")
                _ticketId = str(input(" > "))
            self.getTicketfromAPI(_tickdid, _ticketId)
        pass

    def addTicketToLocal(self, _ticketObject={}):
        """ add a ticket to the ticket object """
        if _ticketObject != {}:
            self.TicketArray.append(_ticketObject)
            return True
        else:
            return False
        pass

    def readTicketList(self, _print=False):
        """ read all tickets that are saved in this instance """
        if _print:
            for e in self.TicketArray:
                print(e)
                print("{} - {}".format(e["shortCode"], e["ticket"]["title"]))
            return True
        else:
            return self.TicketArray

    def removeTicketFromLocal(self, _shortCode=""):
        """ removes the ticket by it shortCode """
        if _shortCode != "":
            res = tickd.tickdUtils.findArrayLocation(self.TicketArray, "shortCode", _shortCode)
            if type(res) == type(0):
                self.TicketArray.pop(res)
                return True
            else:
                self.removeTicketFromLocal()
        else:
            print("enter the id of the tickd instance you want remove")
            tId = str(input("tickd id > "))
            self.removeTicketFromLocal(tId)

    def getTicketbyShortCode(self, _shortcode="", _print=True):
        """ intellgently find a ticket weather it is saved localay or though api and outputs"""
        if _shortcode != "":
            # shearch though the local for short code.
            for e in self.TicketArray:
                # print(e["shortCode"])
                if e["shortCode"] == "{}".format(_shortcode):
                    return e
            # pull apart the short code to and get the ticket from the api
            t = tickd.tickdUtils.breakDownShortcode(_shortcode)
            ticket = self.getTicketfromAPI(t[0], t[1])
            self.addTicketToLocal(ticket)
            if _print: # boolen
                tickd.tickdUtils.readTicketSingle(ticket)
                if tickd.tickdUtils.yesNo("would you like to see this ticket in chrome : "): #returns a boolen
                    tlocal = tickd.tickdUtils.findArrayLocation(self.TickdArray, "id", t[0])
                    tickd.tickdUtils.openTicketinCrome(self.TickdArray[tlocal]['url'], t[1])
            pass

            return ticket
        else:
            _shortcode = str(input("enter a shortCode"))
            self.getTicketbyShortCode(_shortcode)
        pass

    def reAsignTicket(self, _shortcode=""):
        """ reasigns a ticket to a user based on a list of people who assigned the ticket to someone else """
        if _shortcode != "":
            # get ticket object
            ticket = self.getTicketbyShortCode(_shortcode, False)
            # get the current Assignment
            currentAssignment = ticket['ticket']['assignedTo']
            # get the choices for reasignment - aka everone who has assigned the ticket
            print("here are the people you can reasign this ticket to ;")
            temp = {} #stores id as key and and users name as value
            idArray = [] # an array of the ids of users to loop though the print
            for e in ticket['ticket']['assignmentHistory']:
                if e['assigned by id'] in idArray:
                    pass
                else: #if the id is allready in the array then do nothing if it not the add it to the dict and the array
                    temp[e['assigned by id']] = e['assigned by']
                    idArray.append(e['assigned by id'])
            for e in idArray:
                print("{} - {}".format(e, temp[e]))
            print("plese enter the id of the person who you wish to assign this ticket too:")
            idVal = 0
            # reasign Ticket
            while idVal == 0:
                idInput = int(input("int >>"))
                if idInput in idArray:
                    idVal = 1
                pass
            brokenDownSortcode = tickd.tickdUtils.breakDownShortcode(_shortcode)
            tickdInst = {}
            for e in self.TickdArray:
                if e["id"] == brokenDownSortcode[0]:
                    tickdInst = e
            try:
                response = tickd.tickdAPI.putTicketAssigned( e['url'], e['key'], brokenDownSortcode[-1], idInput)
                if response['statuscode'] == 200: #200 is http errorcode for sucess
                    print("the Ticket has bee reasgined")

                pass
                return True
            except Exception as e:
                print("opps - somthing has gone wrong")
                print("error: {}".format(e))
            # update local Object
