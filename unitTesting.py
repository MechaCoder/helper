import unittest
import time

import utills
import tasklist

taskObject = tasklist.TaskList()

class utillsTest(unittest.TestCase):
    #utills tests
    def testTimeOfDay(self):
        print("testing | utills.timeOfDay")
        self.assertEqual(utills.timeOfDay(6), "morning")
        self.assertEqual(utills.timeOfDay(12), "afternoon")
        self.assertEqual(utills.timeOfDay(19), "evening")
        print("testing | utills.timeOfDay > done")
        pass

    def testGetHour(self):
        print("testing | utills.getHour")
        self.assertEqual(utills.getHour(), int(time.strftime("%H")) )
        print("testing | utills.getHour > done")
        pass

    #TaskList
    def testAddItem(self):
        print("testing | taskList.TaskList.addItem")
        self.assertEqual(taskObject.addItem("todo", "test1"), "test1 has been added to the todo list")
        self.assertEqual(taskObject.addItem("underway", "test2"), "test2 has been added to the underway list")
        self.assertEqual(taskObject.addItem("done", "test3"), "test3 has been added to the done list")
        self.assertEqual(taskObject.__str__(), "You have 1 tasks in toDo, you have 1 underway and you have done 1 tasks.")
        print("testing | taskList.TaskList.addItem > done")
        pass

    def testDeleteItem(self):
        print("testing | taskList.TaskList.DeleteItem")
        taskObject.deleteItem("todo", "test1")
        self.assertEqual(taskObject.__str__(), "You have 0 tasks in toDo, you have 1 underway and you have done 1 tasks.")
        taskObject.deleteItem("done", "test3")
        self.assertEqual(taskObject.__str__(), "You have 0 tasks in toDo, you have 1 underway and you have done 0 tasks.")
        print("testing | taskList.TaskList.DeleteItem > Done")
        pass

    def testOne(self):
        self.assertEqual(tickdObj.loadTickdInstance("test", "test.tickd","testKey"), True)
        self.assertEqual(type(tickdObj.TickdArray), type([{"id": "test", "url": "test.tickd", "key": "testKey"}]))

    def testTwo(self):
        # tickdObj.loadTickdInstance("test", "test.tickd","testKey")
        self.assertEqual( type(tickdObj.readTickdInstance(False)), type([]) )
        self.assertEqual(tickdObj.readTickdInstance(True), True)

    def testThree(self):
        self.assertEqual(tickdObj.removeTickdInstance("test"), True)
        self.assertEqual(tickdObj.removeTickdInstance("test"), True)
        self.assertEqual(len(tickdObj.TickdArray), 0)
        pass

    def testFour(self):
        self.assertEqual(tickdObj.loadTickdInstance("local", "local.tickd","F96280A0-DAC4-421B-BA120EB9D0F3E2AC"), True)
        self.assertEqual(type(tickdObj.getTicketfromAPI("local", "1136")), type({}))
        pass

    def testFive(self):
        self.assertEqual(tickdObj.addTicketToLocal({"attempt": True}), True)
        self.assertEqual(tickdObj.addTicketToLocal({"attempt": True}), True)
        pass

    def testSix(self):
        self.assertEqual(type(tickdObj.readTicketList(False)), type([]) )
        tickdObj.TicketArray = [{"shortCode": "try", "ticket": {"title": "bob"}}]
        self.assertEqual(type(tickdObj.readTicketList(True)), type(False) )
        pass

    def testSeven(self):
        tickdObj.TicketArray = [{"shortCode": "try - bob", "ticket": {"title": "bob"}}]
        tickdObj.removeTicketFromLocal("try - bob")
        self.assertEqual( len(tickdObj.TicketArray), 0 )
        pass

    def testEight(self):
        tickdObj.TicketArray = [{"shortCode": "try - bob2", "ticket": {"title": "bob2"}},{"shortCode": "try - bob", "ticket": {"title": "bob"}}]
        self.assertEqual(tickdObj.getTicketbyShortCode("try - bob2"), {"shortCode": "try - bob2", "ticket": {"title": "bob2"}})
        pass

print("[test start ]")
if __name__ == '__main__':
    unittest.main()
