import utills

class TaskList(object):
    """ holding functions to operate TaskList | init & str"""

    def __init__(self):
        self.todo = []
        self.underway = []
        self.done = []

    def __str__(self):
        return "You have {} tasks in toDo, you have {} underway and you have done {} tasks.".format(
            len(self.todo),
            len(self.underway),
            len(self.done)
        )

    def addItem(self, _list="", _item=""): #creates a task in one of the list
        """ addItem this adds an to a an list (todo | underway | done), and item as a string  """
        if len(_list) != 0 and len(_item) != 0:
            if _list == "todo":
                self.todo.append( _item )
                return("{} has been added to the todo list".format(_item))
            elif _list == "underway":
                self.underway.append(_item)
                return("{} has been added to the underway list".format(_item))
            elif _list == "done":
                self.done.append(_item)
                return("{} has been added to the done list".format(_item))
        else:
            if len(_list) == 0:
                print("what list would you like to add too")
                listChoice = str( raw_input("todo | underway | done >") )
            if len(_item) == 0:
                print("what is the text of the item")
                itemText = str( raw_input("[string] >") )
            self.addItem(listChoice, itemText)
        pass

    def readList(self, _list="", _bullpoint="> "):
        """ prints out a list based on the list string. """
        if _list == "todo" or _list == "underway" or _list == "done":
            if _list.lower() == "todo":
                for e in self.todo:
                    print("{} {} ".format(_bullpoint,e))
            elif _list.lower() == "underway":
                for e in self.underway:
                    print("{} {} ".format(_bullpoint, e))
            elif _list.lower() == "done":
                for e in self.done:
                    print("{} {}".format(_bullpoint, e))
            else:
                self.readList("", _bullpoint)
            pass
        else:
                print("pleses choice a list")
                listinput = str(raw_input("todo | underway | done > "))
                self.readList(listinput)

    def moveItem(self, _start="", _end="", _item=""):
        """ moves the item form one list anouther, _start the start list, _end the the ending list """
        if len(_start) != 0 and len(_end) != 0 and len(_item) != 0:
            listString = "todo | underway | done"
            #deletes the item for the the orginal
            if _start.lower() == "todo":
                if _item.lower() in self.todo:
                    self.todo.remove(_item.lower())
                else:
                    print("item not found")
                    return False
            elif _start.lower() == "underway":
                if _item.lower() in self.underway:
                    self.todo.remove(_item.lower())
                else:
                    print("item not found")
                    return False
            elif _start.lower() == "done":
                if _item.lower() in self.done:
                    self.done.remove(_item.lower())
                else:
                    print("item not found")
                    return False
            else:
                print("invaild list choice")
                print("pleses select an orginal list")
                temp = str(raw_input(listString))
                self.moveItem(temp, _end, _item)
            #assign item to a new item
            if _end.lower() == "todo":
                self.todo.append(_item)
            elif _end.lower() == "underway":
                self.underway.append(_item)
            elif _end.lower() == "done":
                self.done.append(_item)
            else:
                print("invaild list choice")
                print("pleses select an orginal list")
                temp = str(raw_input(listString))
                self.moveItem(_start, temp, _item)
        else: #if there is a len to any arguments
            if len(_start) == 0:
                print("pleses select an orginal list")
                startTemp = str(raw_input(listString))
            else:
                startTemp = _start

            if len(_end) == 0:
                print("pleses select an end list")
                endTemp = str(raw_input(listString))
            else:
                endTemp = _end

            if len(_item) == 0:
                print("pleses enter the text of the item you want to move")
                itemTemp = str(raw_input(listString))
            else:
                itemTemp = _item

            self.moveItem(startTemp, endTemp, itemTemp )

    def deleteItem(self, _list="", _item=""):
        """ removes an item form a list """
        if len(_list) != 0 and len(_item) != 0:
            if _list.lower() == "todo":
                if _item in self.todo:
                    self.todo.remove( _item )
            elif _list.lower() == "underway":
                if _item in self.underway:
                    self.underway.remove(_item)
            elif _list.lower() == "done":
                if _item in self.done:
                    self.done.remove(_item)
        else:
            if len(_list) == 0:
                print("select a list list")
                _list = str(raw_input("todo | underway | done"))

            if len(_list) == 0:
                print("enter the item")
                _item = str(raw_input(" str > "))
            self.deleteItem( _list, _item )

    def welcome(self):
        loopExit = True
        while loopExit:
            print("Hi what would you like to do;")
            temp = str( raw_input("add Item | read list | move item | delete | exit > ") )
            if temp.lower() == "add item":
                self.addItem()
            elif temp.lower() == "read list":
                self.readList()
            elif temp.lower() == "move item":
                self.moveItem()
            elif temp.lower() == "delete item":
                self.deleteItem()
            elif temp.lower() == "exit":
                print(" are you sure that you wish to exit ")
                exitTemp=  str(raw_input("Y|y|N|n > "))
                if exitTemp.lower() == "y":
                    print("goodbye")
                    loopExit = False
                    break
                #end if
            #end if
        #end while
